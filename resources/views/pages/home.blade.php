@extends('layout.page')

@section('title', $title)
@section('description', $description)

@section('page')
  <section id='home' class="relative p-5 py-32 bg-cover bg-dark bg-blend-overlay" style="background-image: url('/assets/img/bg.jpg');">
    {{-- <div class="absolute top-0 w-full h-full bg-dark bg-opacity-90"></div> --}}
    
    <div class="container mx-auto">
      <div class="flex flex-col items-center justify-center">
        <div class="px-5 mb-5 text-center">
          <img src="/assets/img/IPL.png" alt="ipl logo" class="h-32 mx-auto mb-4">
          <div class="text-xl text-white text-cent">Compete, Collaborate, Celebrate.</div>
        </div>
        {{-- <div class="px-5 mb-4 text-3xl font-bold text-center md:text-4xl">
          <div id="text-slider" >
            <div class="w-full py-4">
              <div class="w-full text-white ">IEDC Summit - Gamified!</div>
            </div>
            <div class="w-full py-4">
              <div class="w-full text-white ">The battle between IEDC's</div>
            </div>
            <div class="w-full py-4">
              <div class="w-full text-white ">Hackathons</div>
            </div>
            <div class="w-full py-4">
              <div class="w-full text-white ">Workshops</div>
            </div>
            <div class="w-full py-4">
              <div class="w-full text-white ">Bootcamps</div>
            </div>
            <div class="w-full py-4">
              <div class="w-full text-white ">Demo Days</div>
            </div>
            <div class="w-full py-4">
              <div class="w-full text-white ">Startups Pitches</div>
            </div>
          </div>
        </div> --}}
      </div>
      
    </div>
    
  </section>
  <section class="p-5 py-24 bg-pink-100">
    <div class="container mx-auto">
      <div class="flex flex-col items-center md:flex-row">
        <div class="mx-auto mb-10 text-justify md:max-w-5xl md:w-1/2 md:px-12 md:mb-0">
          <div class="mb-3 text-4xl font-bold">About IPL</div>
          <div class="w-24 h-1 mb-8 bg-pink-400"></div>
          <p class="mb-4">
            Innovators' Premier League (IPL) is an initiative of the Kerala Startup Mission (KSUM) to bring out the best talents from the Kerala Innovation Hubs.
            IPL aims to bring awareness and sensitization of IEDC & its activities among students, building a competitive & entrepreneurial mindset among innovators, and create the pipeline of startups.
          </p>
          <p class="mb-8">
            During IPL, the students and other talents in the institution will compete with each other at the College level, followed by Regional level competitions under the 3 major pillars - Innovation, Technology & Entrepreneurship. The league competition runs on a point scale where institutions and students can achieve points through conducting boot camps, workshops, startup generation, patents, etc. under various categories.
          </p>
          {{-- <a href="https://www.instagram.com/iedc_kerala/" target="_blank" class="inline-block px-5 py-2 text-white transform bg-black border-4 border-white rounded-full shadow">
            View Latest Updates
          </a> --}}
        </div>
        <div class="px-10">
          <div id="img-slider" >
            <div>
              <img src="/assets/img/img1.png" alt="iedc summit">
            </div>
            <div>
              <img src="/assets/img/img2.png" alt="iedc summit">
            </div>
            <div>
              <img src="/assets/img/img3.png" alt="iedc summit">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="py-16 text-center text-white bg-gray-900">
    <div class="container mx-auto">
      <div class="flex flex-wrap">
        @foreach ($stats as $item)
          <div class="w-1/2 py-10 md:w-1/5">
            <div class="text-5xl font-bold flex justify-center items-center">
              <p class='stats-item'>{{$item->count}}</p>
              <sup>
                {{$item->sup}}
              </sup>
            </div>
            <div class="text-sm font-light">
              <p class="whitespace-pre-wrap">{{$item->text}}</p> 
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>

  <section id='timeline' class="px-5 py-24">
    <div class="container mx-auto">
      <div class="pb-10 text-3xl font-bold text-center">IPL Challenges Timeline</div>
      <p class="mb-24 text-justify">The Innovators Premier League commences on 18 September 2021 followed by IEDC event Plan submission by each IEDC and the events ending on Dec 30, 2021, with a handful of winners who will get exposure to the Business Tycoons and Ecosystem enablers during the next IEDC Summit.</p>
      <div class="flex flex-col items-center justify-center timeline md:flex-row">

        @foreach ($timeline as $item)
            
          <div class="relative w-full item md:w-1/4">
            <div class="flex flex-row text-center content md:flex-col">
              <div class="pb-10">
                <div class="absolute transform rotate-90 flag left-1/2 top-1/2 md:top-auto md:bottom-full md:rotate-0">
                  <canvas data-flag="{{$item->image}}"></canvas>
                </div>
              </div>
              <div class="w-1/2 py-16 pl-10 md:w-auto md:py-0 md:px-0">
                <div class="date md:mt-10">{{$item->date}}</div>
                <div class="font-bold text-pink-700">{{$item->text}}</div>
              </div>
            </div>
            <div class="absolute w-1 h-full transform -translate-x-1/2 -translate-y-1/2 bg-pink-600 line md:h-1 md:w-full top-1/2 left-4 md:left-1/2"></div>
            <div class="absolute w-6 h-6 transform -translate-x-1/2 -translate-y-1/2 bg-pink-700 rounded-full round top-1/2 left-4 md:left-1/2"></div>
          </div>

        @endforeach
      </div>
    </div>
  </section>

  <!-- <section class="relative">
    <canvas id="particle-canvas" class="absolute top-0 left-0 w-full h-full -z-10"></canvas>
    <div class="top-0 left-0 w-full h-full py-24 ">
      <div class="container mx-auto text-white">
        <div class="mb-8 text-3xl font-bold text-center">Awards out to grab</div>
        @foreach ($awards as $award)
          <div class="flex flex-wrap items-center justify-center text-center text-blue-800">
            @foreach ($award as $item)
                
              <div class="p-3">
                <div class="p-3 px-5 bg-white rounded-md">
                  {{$item->text}}
                </div>
              </div>
            
            @endforeach
          </div>
        @endforeach
      </div>
    </div>
  </section> -->

  <section id='amb' class='mb-2'>
    <div class="container mx-auto">
      <div class="pb-10 text-3xl font-bold text-center">IPL Ambassadors</div>
      <div class="amb-carousel overflox-x-scroll whitespace-nowrap">
        @foreach ($ambassadors as $ambassador)
        <div class="w-1/4 inline-block pb-1 amb-carousel-item">
            <img src={{$ambassador->poster}} alt="">
        </div>
        @endforeach
      </div>
    </div>
  </section>
  
  <section id='awards'  class='mb-20 mt-20'>
    <div class="container mx-auto">
      <div class="pb-10 text-3xl font-bold text-center">Student Awards</div>
      <div class="arrow-wrapper relative">
        <img class='carousel-arrow ca-left' src="assets/img/arrow.png" alt="">
        <img class='carousel-arrow ca-right' src="assets/img/arrow.png" alt="">
        <div id="carousel" class="overflow-x-scroll whitespace-nowrap relative" >
        
          <!-- <div class="w-full h-8 rounded-full "></div> -->
          @foreach ($winners as $winner)
          <div class="inline-block pb-1 carousel-item">
            <img src={{$winner->path}} alt="">
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </section>

  <section  class='mb-20 mt-20'>
    <div class="container mx-auto">
      <div class="pb-10 text-3xl font-bold text-center">Institutional Awards</div>
      <div class="arrow-wrapper relative">
        <img class='carousel-arrow ca-left' src="assets/img/arrow.png" alt="">
        <img class='carousel-arrow ca-right' src="assets/img/arrow.png" alt="">
        <div id="carousel" class="overflow-x-scroll whitespace-nowrap" >
          <!-- <div class="w-full h-8 rounded-full "></div> -->
          @foreach ($institutions as $institution)
          <div class="inline-block pb-1 carousel-item">
            <img src={{$institution->path}} alt="">
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </section>

  <section id=''>
    <div class="container mx-auto resource-div">
      <div class='resource-grid-box'>
        <a href="https://drive.google.com/file/d/1f-hSbxmvph-lwjIX00mpQlSEE0p6mtb4/view?usp=sharing" target='blank'>Institutional Awards</a>
        <a href="https://drive.google.com/file/d/1R7Teu6tUiigicHbA-rGbTD0ulUFt0vuh/view?usp=sharing" target='blank'>Student Awards</a>
        <a href="https://drive.google.com/file/d/1dhb9sriHQipLjb7I7N0u3HZ213q7Ac1m/view" target='blank'>IPL Challenges Awards</a>
      </div>
    </div>
  </section>

  <section id='leaderboard' class="px-5 py-24">
    <div class="container mx-auto">
      <div class="pb-10 text-3xl font-bold text-center">IPL Leaderboard</div>
      <div class="text-2xl font-bold text-center">Colleges</div>
      <div class="mb-10">
        <iframe class="airtable-embed" src="https://airtable.com/embed/shrWbufKirpg7kiFU?backgroundColor=purple&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>
      </div>
      <div class="text-2xl font-bold text-center">Students</div>
      <div class="mb-10">
        <iframe class="airtable-embed" src="https://airtable.com/embed/shr3g57JOd2ugXVtB?backgroundColor=green&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>
      </div>

      <div>
        <div class="font-bold mb-4">Note:</div>
        <ul class="list-disc">
          <li>Points are awarded for events updated before 30 Dec 2021, 11:59 PM</li>
          <li>The decisions of the organizing team are final.</li>
          <li>The awards are given based on the criteria met as instructed during the launch of IPL.</li>
          <li>The scores shown are final. Scores are updated as per data uploaded to Innovate Portal by the IEDCs.</li>
          <li>KSUM reserves the right to alter or withdraw any clause of the contest or the contest itself without any prior notice. In such an event post facto notification will be sent to levels at or above Nodal Officers by KSUM IEDC team.</li>
        </ul>
      </div>
    </div>
  </section>
  <section id='resources'>
    <div class="container mx-auto resource-div">
      <div class="pb-10 text-3xl font-bold text-center">Resources</div>
      <div class='resource-grid-box resorce-box-down'>
        <a href="https://drive.google.com/drive/folders/1BE57cGJUv89khUTsHE73WAL46HoI2Ipo?usp=sharing" target='blank'>Logos and Creatives</a>
        <a href="https://drive.google.com/file/d/1i_XEaFpQT2mbRZQmdmG9gAy4dPw4_hVf/view" target='blank'>Prayatna Points</a>
        <a href="https://drive.google.com/drive/folders/1C_Ce7H1gXPgKh83aLwd5lzJpKWXrBo0F" target='blank'>IPL Points</a>
        <a href="https://drive.google.com/drive/folders/1WzBszDMWcbCT5txDXbJVZU-LUsEuEZwe" target='blank'>Reference Templates</a>
      </div>
    </div>
  </section>
  <section id="contact">
    <div class="footer">
      <div class="footer-add">
        <div class="footer-p">
            <p><span class="add">Address: </span>Thejaswini, G3B, Technopark Rd, Karyavattom, Thiruvananthapuram, Kerala 695581</p>
            <p><span class="add">Phone: </span>0471-2700270</p>
            <p><span class="add">Email: </span>iedckerala@startupmission.in</p>
        </div>
      </div>
      <div class="logo">
        <a href="https://www.facebook.com/keralastartupmission" target="blank"><img src="/assets/img/fb.png" alt="">  </a>
        <a href="https://www.instagram.com/iedc_kerala/" target="blank"><img src="/assets/img/ig.png" alt="">    </a>
        <a href="https://www.linkedin.com/company/kerala-startup-mission/" target="blank"><img src="/assets/img/li.png" alt="">  </a>
        <a href="https://twitter.com/startup_mission" target="blank"><img src="/assets/img/tw.png" alt=""> </a>
        <a href="mailto:iedckerala@startupmission.in" target="blank"><img src="/assets/img/gm.png" alt=""></a>  
      </div>

    </div>
    </section>





  <!-- <section class="pb-24">
    <div class="container mx-auto">
      <div class="mb-5 text-3xl font-bold text-center">IPL Awards</div>
      <div class="space-x-4 text-center">
        <a target="_blank" href="https://drive.google.com/file/d/1xWLODlfU8oIeeBji4gMYVevCtDfNGXws/view?usp=sharing" class="inline-block px-5 py-2 text-white transform bg-black border-4 border-white rounded-full shadow">
          IPL challenges
        </a>
        <a target="_blank" href="https://drive.google.com/file/d/1f_0if89jxCAR0w1cAuWnphLxvfVguBPN/view?usp=sharing" class="inline-block px-5 py-2 text-white transform bg-black border-4 border-white rounded-full shadow">
          Institution
        </a>
        <a target="_blank" href="https://drive.google.com/file/d/1kWOqfTRsmmpVOechye7Mb-UJnEyGWTJP/view?usp=sharing" class="inline-block px-5 py-2 text-white transform bg-black border-4 border-white rounded-full shadow">
          Students
        </a>
      </div>
    </div>
  </section> -->

  {{-- <section class="px-5 py-24">
    <div class="container mx-auto">

      <div class="pb-10 text-3xl font-bold text-center">Prayatna Points</div>

      <p class="mb-3 text-justify">IPL encourages healthy competition, and the immediate incentives range from gift vouchers to goodies for racking up a certain number of ‘Prayatna Points’. It provides a simple and straightforward achievement system to motivate yourself & your IEDC.</p>
      <div class="mb-24 text-justify">Innovators are rewarded with prayatna points based on the various factors such as taking part in events, winning challenges, innovator referral etc, which will be contributed to the parent institution as well and the institution can earn Silver, Gold, or Platinum rating based on their performance. This ensures more engagement, creates a competitive yet collaborative team environment.</div>

      <div class="pb-10 text-3xl font-bold text-center">Leaderboard</div>

      <div class="mb-20">
        <div class="text-2xl font-bold text-center">Colleges</div>
        <div class="text-sm text-center">(Top 10)</div>
        <div id="collegeboard"></div>
        <div class="text-center">
          <a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vS556BEmstTpGCC8AqAaXw8L4M95OFgnrzeM5pd8GuvFJwJTqXX_kVdJfVHub4eY4dSoCamvBj60m7s/pubhtml?gid=115828790&single=true" target="_blank" class="text-blue-500 hover:text-blue-400">View All</a>
        </div>
      </div>
      
      <div class="text-2xl font-bold text-center">Students</div>
      <div class="text-sm text-center">(Top 20)</div>
      <div id="leaderboard"></div>
      <div class="text-center">
        <a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vS556BEmstTpGCC8AqAaXw8L4M95OFgnrzeM5pd8GuvFJwJTqXX_kVdJfVHub4eY4dSoCamvBj60m7s/pubhtml?gid=1744278839&single=true" target="_blank" class="text-blue-500 hover:text-blue-400">View All</a>
      </div>
    </div>
  </section> --}}

  {{-- <section class="pb-24">
    <div class="container mx-auto">
      <div class="mb-5 text-3xl font-bold text-center">Resources</div>
      <div class="space-x-4 text-center">
        <a href="https://drive.google.com/drive/folders/1BE57cGJUv89khUTsHE73WAL46HoI2Ipo?usp=sharing" class="inline-block px-5 py-2 text-white transform bg-black border-4 border-white rounded-full shadow">
          Logos
        </a>
        <a href="https://drive.google.com/drive/folders/1C_Ce7H1gXPgKh83aLwd5lzJpKWXrBo0F?usp=sharing" class="inline-block px-5 py-2 text-white transform bg-black border-4 border-white rounded-full shadow">
          IPL Presentation
        </a>
        <a href="https://drive.google.com/drive/folders/1WzBszDMWcbCT5txDXbJVZU-LUsEuEZwe?usp=sharing " class="inline-block px-5 py-2 text-white transform bg-black border-4 border-white rounded-full shadow">
          Reference Templates
        </a>
      </div>
    </div>
  </section> --}}

  {{-- <section>
    <style>
      .referral{
        display: none !important;
      }
    </style>
    <script src="https://assets.juicer.io/embed.js" type="text/javascript"></script>
    <ul class="juicer-feed" data-feed-id="iedc_kerala">
    </ul>
  </section> --}}

@endsection

@push('style')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/tiny-slider.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
@endpush

@push('script')
  {{-- <script src="{{$mix['/assets/js/leaderboard.js']}}" defer></script> --}}
  {{-- 
    <script>
      
    </script> 
  --}}
@endpush

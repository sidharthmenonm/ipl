import Leaderboard from '../svelte/Leaderboard.svelte';
import Collegeboard from '../svelte/Collegeboard.svelte';

new Leaderboard({
  target: document.querySelector('#leaderboard'),
})

new Collegeboard({
  target: document.querySelector('#collegeboard'),
})
@extends('layout.app')

@section('content')
  <div class="w-full min-h-screen flex" x-data="{main_menu:false}">
    @include('layout.nav')
    
    <main class="w-full">
      <div class="flex sm:hidden p-3 bg-white shadow">        
        <img src="/assets/img/logo.svg" alt="Logo" class="object-contain h-12">
        <button @click="main_menu = !main_menu" class="cursor-pointer fixed right-5 w-10 h-10 text-xl ml-auto rounded transition duration-150 hover:shadow leading-none px-2 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none" type="button">
          <span class="block absolute w-6 h-0.5 rounded-sm bg-gray-600 top-3 duration-500" :class="[main_menu?'opacity-0':'']"></span>
          <span class="block absolute w-6 h-0.5 rounded-sm bg-gray-600 duration-500" :class="[main_menu?'transform rotate-45':'']"></span>
          <span class="block absolute w-6 h-0.5 rounded-sm bg-gray-600 duration-500" :class="[main_menu?'transform -rotate-45':'']"></span>
          <span class="block absolute w-6 h-0.5 rounded-sm bg-gray-600 bottom-2.5 duration-500" :class="[main_menu?'opacity-0':'']"></span>
        </button>
      </div>
      
      @yield('page')

      
      {{-- <section class="py-16 px-8 sm:px-24" style="background-color: #021232;">
        <div class="flex flex-col sm:flex-row justify-between items-center">
          <div class="flex flex-col items-center text-white text-center">
            <img src="/assets/img/logo-white.svg" alt="KSUM Logo" class="w-48">
            <p>G3B, Thejaswini, Technopark Campus</p>
            <p>Kariyavattom, Trivandrum, Kerala 695581</p>
          </div>
          <div class="flex flex-col items-center text-white mt-20 sm:m-0">
            
          </div>
        </div>
      </section> --}}
      <section class="p-5 text-center text-white" style="background-color: #010b1d;">
        Copyright © 2021 Kerala Startup Mission. All Rights Reserved.
      </section>
    </main>
    
  </div>
@endsection
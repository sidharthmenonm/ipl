<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  {{-- <title>{{config('app.name')}}</title> --}}

  <!-- Primary Meta Tags -->
  <title>@yield('title')</title>
  <meta name="title" content="@yield('title')">
  <meta name="description" content="@yield('description')">

  <!-- Open Graph / Facebook -->
  <meta property="og:type" content="website">
  <meta property="og:url" content="https://startupmission.kerala.gov.in">
  <meta property="og:title" content="@yield('title')">
  <meta property="og:description" content="@yield('description')">
  <meta property="og:image" content="/assets/img/ksum_logo1.png">

  <!-- Twitter -->
  <meta property="twitter:card" content="summary_large_image">
  <meta property="twitter:url" content="https://startupmission.kerala.gov.in">
  <meta property="twitter:title" content="@yield('title')">
  <meta property="twitter:description" content="@yield('description')">
  <meta property="twitter:image" content="/assets/img/ksum_logo1.png">

  <link href="https://assets.juicer.io/embed.css" media="all" rel="stylesheet" type="text/css" />
  <link href="{{$mix['/assets/css/app.css']}}" rel="stylesheet">
  
  <link rel="preconnect" href="https://fonts.gstatic.com">
  
  @yield('style')
  @stack('style')
</head>
<body class="overflow-x-hidden ">

  @yield('content')
  
  <script src="{{$mix['/assets/js/app.js']}}" defer></script>
  
  @yield('script')
  @stack('script')
</body>
</html>
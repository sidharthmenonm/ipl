<div id="navbar" :class="[main_menu?'menu-open':'']" 
  class="fixed top-0 left-0 z-10 w-64 min-h-screen p-8 transition-all duration-300 ease-in-out transform -translate-x-full bg-indigo-900 sm:w-full sm:min-h-0 sm:h-20 sm:py-3 sm:flex sm:items-center sm:justify-between sm:translate-x-0 md:bg-transparent ">
  
  
  <div class="flex items-center justify-center p-5 mb-5 font-medium bg-white rounded-md sm:m-0 sm:bg-transparent sm:p-0">
    <img src="/assets/img/logo.svg" class="sm:hidden" alt="Logo">
    <img src="/assets/img/logo-white.svg" class="hidden sm:block sm:h-16" alt="Logo">
  </div>

  <nav>
    <ul class="sm:flex md:text-sm">

      <li class="">
        <a href="/" class="px-2 py-1 text-white md:text-white-500">Home</a>
      </li>
      <li class="">
        <a href="#timeline" class="px-2 py-1 text-white md:text-white-500">Timeline</a>
      </li>
      <li class="">
        <a href="#amb" class="px-2 py-1 text-white md:text-white-500">Ambassadors</a>
      </li>
      <li class="">
        <a href="#awards" class="px-2 py-1 text-white md:text-white-500">Awards</a>
      </li>
      <li class="">
        <a href="#leaderboard" class="px-2 py-1 text-white md:text-white-500">Leaderboard</a>
      </li>
      <li class="">
        <a href="#resources" class="px-2 py-1 text-white md:text-white-500">Resources</a>
      </li>
      <li class="">
        <a href="#contact" class="px-2 py-1 text-white md:text-white-500">Contact</a>
      </li>
      

    </ul>
  </nav>

</div>


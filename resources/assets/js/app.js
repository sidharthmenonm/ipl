import Alpine from 'alpinejs'

window.Alpine = Alpine
Alpine.start();

window.addEventListener("scroll", function() {
  if (window.scrollY > 100) {
    document.querySelector("#navbar").classList.add('md:bg-indigo-900')
  } else {
    document.querySelector("#navbar").classList.remove('md:bg-indigo-900')
  }
}, false);

import {tns}  from 'tiny-slider/src/tiny-slider';

// var slider1 = tns({
//   container: '#text-slider',
//   mode: 'gallery',
//   controls: false,
//   autoplay: true,
//   nav: false,
//   autoplayButtonOutput: false,
//   "animateIn": "animate__backInDown",
//   "animateOut": "animate__backOutRight",
//   axis: 'vertical',
//   autoplayTimeout: 3000,
// })

var slider2 = tns({
  container: '#img-slider',
  mode: 'gallery',
  controls: false,
  autoplay: true,
  nav: false,
  autoplayButtonOutput: false,
  autoplayTimeout: 3000,
})

// modified version of random-normal
function normalPool(o){var r=0;do{var a=Math.round(normal({mean:o.mean,dev:o.dev}));if(a<o.pool.length&&a>=0)return o.pool[a];r++}while(r<100)}function randomNormal(o){if(o=Object.assign({mean:0,dev:1,pool:[]},o),Array.isArray(o.pool)&&o.pool.length>0)return normalPool(o);var r,a,n,e,l=o.mean,t=o.dev;do{r=(a=2*Math.random()-1)*a+(n=2*Math.random()-1)*n}while(r>=1);return e=a*Math.sqrt(-2*Math.log(r)/r),t*e+l}

const NUM_PARTICLES = 600;
const PARTICLE_SIZE = 0.5; // View heights
const SPEED = 20000; // Milliseconds

let particles = [];

function rand(low, high) {
  return Math.random() * (high - low) + low;
}

// function createParticle(canvas) {
//   const colour = {
//     r: 255,
//     g: randomNormal({ mean: 125, dev: 20 }),
//     b: 50,
//     a: rand(0, 1),
//   };
//   return {
//     x: -2,
//     y: -2,
//     diameter: Math.max(0, randomNormal({ mean: PARTICLE_SIZE, dev: PARTICLE_SIZE / 2 })),
//     duration: randomNormal({ mean: SPEED, dev: SPEED * 0.1 }),
//     amplitude: randomNormal({ mean: 16, dev: 2 }),
//     offsetY: randomNormal({ mean: 0, dev: 10 }),
//     arc: Math.PI * 2,
//     startTime: performance.now() - rand(0, SPEED),
//     colour: `rgba(${colour.r}, ${colour.g}, ${colour.b}, ${colour.a})`,
//   }
// }

// function moveParticle(particle, canvas, time) {
//   const progress = ((time - particle.startTime) % particle.duration) / particle.duration;
//   return {
//     ...particle,
//     x: progress,
//     y: ((Math.sin(progress * particle.arc) * particle.amplitude) + particle.offsetY),
//   };
// }

// function drawParticle(particle, canvas, ctx) {
//   canvas = document.getElementById('particle-canvas');
//   const vh = canvas.height / 100;

//   ctx.fillStyle = particle.colour;
//   ctx.beginPath();
//   ctx.ellipse(
//     particle.x * canvas.width,
//     particle.y * vh + (canvas.height / 2),
//     particle.diameter * vh,
//     particle.diameter * vh,
//     0,
//     0,
//     2 * Math.PI
//   );
//   ctx.fill();
// }

// function draw(time, canvas, ctx) {
//   // Move particles
//   particles.forEach((particle, index) => {
//     particles[index] = moveParticle(particle, canvas, time);
//   })

//   // Clear the canvas
//   ctx.clearRect(0, 0, canvas.width, canvas.height);

//   // Draw the particles
//   particles.forEach((particle) => {
//     drawParticle(particle, canvas, ctx);
//   })

//   // Schedule next frame
//   requestAnimationFrame((time) => draw(time, canvas, ctx));
// }

// function initializeCanvas() {
//   let canvas = document.getElementById('particle-canvas');
//   canvas.width = canvas.offsetWidth * window.devicePixelRatio;
//   canvas.height = canvas.offsetHeight * window.devicePixelRatio;
//   let ctx = canvas.getContext("2d");

//   window.addEventListener('resize', () => {
//     canvas.width = canvas.offsetWidth * window.devicePixelRatio;
//     canvas.height = canvas.offsetHeight * window.devicePixelRatio;
//     ctx = canvas.getContext("2d");
//   })

//   return [canvas, ctx];
// }

// function startAnimation() {
//   const [canvas, ctx] = initializeCanvas();

//   // Create a bunch of particles
//   for (let i = 0; i < NUM_PARTICLES; i++) {
//     particles.push(createParticle(canvas));
//   }
  
//   requestAnimationFrame((time) => draw(time, canvas, ctx));
// };

// Start animation when document is loaded
// (function () {
//   if (document.readystate !== 'loading') {
//     startAnimation();
//   } else {
//     document.addEventListener('DOMContentLoaded', () => {
//       startAnimation();
//     })
//   }
// }());


function flag(el, image){ 
  var h = new Image;
  h.onload = function () {
    var flag = el;
    var amp = 10;
    flag.width = h.width;
    flag.height = h.height + amp * 2;
    flag.getContext('2d').drawImage(h, 0, amp, h.width, h.height);
    // flag.style.marginLeft = -(flag.width / 2) + 'px';
    // flag.style.marginTop = -(flag.height / 2) + 'px';
    var timer = waveFlag(flag, h.width / 10, amp);
  };
  h.src = image;

  function waveFlag(canvas, wavelength, amplitude, period, shading, squeeze) {
    if (!squeeze) squeeze = 0;
    if (!shading) shading = 100;
    if (!period) period = 200;
    if (!amplitude) amplitude = 5;
    if (!wavelength) wavelength = canvas.width / 10;

    var fps = 30;
    var ctx = canvas.getContext('2d');
    var w = canvas.width, h = canvas.height;
    var od = ctx.getImageData(0, 0, w, h).data;
    // var ct = 0, st=new Date;
    return setInterval(function () {
      var id = ctx.getImageData(0, 0, w, h);
      var d = id.data;
      var now = (new Date) / period;
      for (var y = 0; y < h; ++y) {
        var lastO = 0, shade = 0;
        var sq = (y - h / 2) * squeeze;
        for (var x = 0; x < w; ++x) {
          var px = (y * w + x) * 4;
          var pct = x / w;
          var o = Math.sin(x / wavelength - now) * amplitude * pct;
          var y2 = y + (o + sq * pct) << 0;
          var opx = (y2 * w + x) * 4;
          shade = (o - lastO) * shading;
          d[px] = od[opx] + shade;
          d[px + 1] = od[opx + 1] + shade;
          d[px + 2] = od[opx + 2] + shade;
          d[px + 3] = od[opx + 3];
          lastO = o;
        }
      }
      ctx.putImageData(id, 0, 0);
      // if ((++ct)%100 == 0) console.log( 1000 * ct / (new Date - st));
    }, 1000 / fps);
  }
}

document.querySelectorAll('[data-flag]').forEach(function (item) {
  var image = item.getAttribute('data-flag');
  flag(item, image);
});

function isInViewPort(element) {
  // Get the bounding client rectangle position in the viewport
  var bounding = element.getBoundingClientRect();

  // Checking part. Here the code checks if it's *fully* visible
  // Edit this part if you just want a partial visibility
  if (
      bounding.top >= 0 &&
      bounding.left >= 0 &&
      bounding.right <= (window.innerWidth || document.documentElement.clientWidth) &&
      bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight)
  ) {
      console.log('In the viewport! :)');
      return true;
  } else {
      console.log('Not in the viewport. :(');
      return false;
  }
}
var element = document.getElementsByClassName('stats-item')
let counted = false
let active = []
window.addEventListener('scroll', function (event) {
  if (isInViewPort(element[0])) {
    if(!counted){
      // update the element display
      counted=true
      let timers = []
      let current_counts = []
      for(let i=0;i<element.length;i++) { 
        if(active[i]!=='active') {
          if(parseInt(element[i].innerHTML)-100 > 0){
            current_counts[i]=parseInt(element[i].innerHTML)-100
          }else{
            current_counts[i]=0
          }
          active[i]='active'
          timers[i]=setInterval(updated,50,element[i],parseInt(element[i].innerHTML),i);
        }
      }
      
      function updated(elem,max,i){
        elem.innerHTML=++current_counts[i];
        if(current_counts[i]===max)
        {
            active[i]='deactive'
            clearInterval(timers[i]);
        }
      }
    }
  }else{
    counted=false
  }
}, false);

let position = 1
let amb_images = document.getElementsByClassName('amb-carousel-item')
function changeImage(r){
  r.style.setProperty('--position', position);
  position++
  if(position>amb_images.length){
    position=1
  }
}


function startCarouselAnimation(){
  var r = document.querySelector('.amb-carousel');
  var bgs = ['#e9ff70','#ffd670','#ff9770','#ff70a6','#90f1ef']
  for(let i=0; i<amb_images.length; i++){
    amb_images[i].style.setProperty('background-color', bgs[Math.floor(Math.random() * 6)]);
    amb_images[i].style.setProperty('--offset', i+1);
  }
  r.style.setProperty('--items', amb_images.length);
  r.style.setProperty('--middle', 4);
  setInterval(changeImage,3000,r);
}

let factor = 3000
let direction="left"

function automate_scroll_left(elem){
  if(elem.scrollLeft==0){
    direction="left"
  }
  if(elem.offsetWidth+elem.scrollLeft >= elem.scrollWidth){
    direction="right"
  }
  if(direction=="left"){
    elem.scrollLeft += document.querySelector('.carousel-item').clientWidth;
  }else{
    elem.scrollLeft -= document.querySelector('.carousel-item').clientWidth;
  }
  window.setTimeout(automate_scroll_left,factor,elem)
  
}


function startStudentCarouselAnimation(){
  let carousel_elem = document.querySelectorAll('#carousel')
  for(let i =0;i<carousel_elem.length;i++){
    automate_scroll_left(carousel_elem[i])
  }
}

(function () {
  if (document.readystate !== 'loading') {
    startCarouselAnimation();
    startStudentCarouselAnimation()
  } else {
    document.addEventListener('DOMContentLoaded', () => {
      startCarouselAnimation();
      startStudentCarouselAnimation()
    })
  }
}());


var leftArrow = document.querySelectorAll('.ca-left')
leftArrow.forEach((elem,i) =>{
  elem.addEventListener('click', () =>{
    factor -= 500
    factor= factor<=0?3000:factor
    direction="right"
    // automate_scroll_right(elem.nextElementSibling.nextElementSibling)
  })
})

var rightArrow = document.querySelectorAll('.ca-right')
rightArrow.forEach((elem,i) =>{
  elem.addEventListener('click', () =>{
    factor -= 500
    factor= factor<=0?3000:factor
    direction="left"
    // automate_scroll_left(elem.nextElementSibling)
  })
})


     


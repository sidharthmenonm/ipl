let mix = require('laravel-mix'),
    build = require('./cleaver.build.js'),
    command = require('node-cmd');

const tailwindcss = require('tailwindcss');
require('laravel-mix-svelte');

mix.disableNotifications();
mix.webpackConfig({
    plugins: [
        build.cleaver
    ],
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        proxy: {
            '/api': {
                target: 'http://localhost:3210',
                pathRewrite: { '^/api': '' },
            },
        },
        hot: true,
        writeToDisk: true,
    }
});

mix.setPublicPath('./')
    .js('resources/assets/js/app.js', 'dist/assets/js')
    .js('resources/assets/js/leaderboard.js', 'dist/assets/js')
    .js('resources/assets/js/details.js', 'dist/assets/js')
    .js('resources/assets/js/students.js', 'dist/assets/js')
    .sass('resources/assets/sass/app.scss', 'dist/assets/css')
    .copyDirectory('resources/assets/img', 'dist/assets/img')
    .options({
        processCssUrls: false,
        postCss: [tailwindcss('./tailwind.config.js')],
    });
    // .svelte();
    // .version();

if (mix.inProduction()) {
    mix.version().svelte();
}
else {
    mix.svelte({
        dev: true
    });
}

mix.browserSync({
    files: [
        "dist/**/*",
        {
            match: ["resources/**/*"],
            fn: function(event, file) {
                command.get('php cleaver build', (error, stdout, stderr) => {
                    console.log(error ? stderr : stdout);
                });
            }
        }
    ],
    proxy: 'localhost:8081'
});
const colors = require('tailwindcss/colors')

module.exports = {
    purge: [
        './resources/views/**/*.php',
        './resources/views/**/*.html',
        './resources/assets/js/**/*.vue',
        './resources/assets/js/**/*.js',
        './resources/content/**/*.html',
        './resources/assets/svelte/**/*.svelte'
    ],
    theme: {
        fontFamily: {
            'sans': ['Poppins', 'ui-sans-serif', 'system-ui', '-apple-system'],
          },
          extend: {
            maxHeight: {
              '1/4': '25%',
              '1/2': '50%',
              '3/4': '75%',
              'half': '50vh',
              'quarter': '25vh',
              '3fourth': '75vh'
            },
            minHeight: {
              '1/4': '25%',
              '1/2': '50%',
              '3/4': '75%',
              'half': '50vh',
              'quarter': '25vh',
              '3fourth': '75vh'
            },
            backgroundSize: {
              '50': '50%',
              '25rem': '25rem'
            },
            colors: {
              transparent: 'transparent',
              current: 'currentColor',
              black: colors.black,
              white: colors.white,
              gray: colors.coolGray,
              red: colors.red,
              yellow: colors.amber,
              green: colors.emerald,
              blue: colors.blue,
              indigo: colors.indigo,
              purple: colors.violet,
              pink: colors.pink,
              'light': '#F3F7FC',
              'dark': '#1e2755',
              'secondary': '#2F2E38',
              'primary': '#0930CC',
            },
            zIndex: {
              '-10': '-10',
            }
        },
    },
    variants: {},
    plugins: [
      require('@tailwindcss/forms'),
    ],
}
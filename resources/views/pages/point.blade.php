@extends('layout.page')

@section('title', $title)
@section('description', $description)

@section('page')
  <div class="min-h-screen">
    <section class="pt-10 pb-10 text-4xl font-bold text-center text-white bg-blue-900">
      <img src="/assets/img/ipl_logo_white.png" alt="ipl logo" class="h-56 mx-auto mb-4">
    </section>

    <section class="py-24">

      <div id="collegedetails"></div>
      
    </section>
  </div>

@endsection

@push('script')
  <script src="{{$mix['/assets/js/details.js']}}" defer></script>
@endpush